def bubble_sort(arr) :
    def swap(i,j) :
        arr[i],arr[j] = arr[j],arr[i]
    
    n = len(arr)
    swapped = True
    
    x = -1 
    while swapped : 
        swapped = False
        x = x + 1
        for i in range(1,n-x) :
            if arr[i-1] > arr[i] :
                swap(i - 1,i)
                swapped = True
    
    return arr
    


if __name__ == "__main__" :
    numbers = list(map(int,input("Enter integer number with space: ")))
    sorted_numbers = bubble_sort(numbers)
    print("Sorted number is", sorted_numbers)
