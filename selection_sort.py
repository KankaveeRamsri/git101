def selection_sort(arr):
    for i in range(len(arr)) :
        minimum = i
        
        for j in range(i + 1,len(arr)):
            if arr[j] < arr[minimum] :
                minimum = j
                
        arr[minimum], arr[i] = arr[i],arr[minimum]
    
    return arr 

if __name__ == "__main__" :
    numbers = list(map(int,input("Enter integer number with space: ")))
    sorted_numbers = selection_sort(numbers)
    print("Sorted number is", sorted_numbers)

